import numpy as np
import pandas as pd
import netCDF4
import datetime
tas_nc_file = 'dataset-projections-cmip5-monthly-single\\tas_Amon_HadGEM2-ES_rcp45_r1i1p1_203012-205511.nc'
nc = netCDF4.Dataset(tas_nc_file, mode='r')
nc.variables.keys()
lat = nc.variables['lat'][:]
lon = nc.variables['lon'][:]
tas = nc.variables['tas'][:]
time_var = nc.variables['time']
dtime = netCDF4.num2date(time_var[:],time_var.units)

## Using Lat[119] = 58.75 and lon[13]=24.375 for Estonia as an example

for t in range(len(dtime)) :
            print(dtime[t],'----',lon[13],'----',lat[119],'-----',(nc.variables['tas'][t,119,13])-273.15)  ## Subtracting 273.15 since the tas kelvin (K) to celsius (°C)
			
